import chess
import uuid

class Zobrist:
    def __init__(self):
        self.table = [[uuid.uuid4().int & (1<<64)-1 for j in range(12)] for i in range(64)]
        self.side = uuid.uuid4().int & (1<<64)-1
        self.en_passant = [uuid.uuid4().int & (1<<64)-1 for i in range(8)]
        self.castling_rights = [uuid.uuid4().int & (1<<64)-1 for i in range(4)]

    def hash(self, board):
        hash = 0
        for square_id in range(64):
            piece = board.piece_at(square_id)
            if piece != None:
                piece_id = piece.piece_type + (6 if piece.color == chess.BLACK else 0) - 1
                hash = hash ^ self.table[square_id][piece_id]

        if board.turn == chess.BLACK:
            hash = hash ^ self.side

        if board.has_legal_en_passant():
            en_passant_file = board.ep_square % 8
            hash = hash ^ self.en_passant[en_passant_file]

        if board.has_kingside_castling_rights(chess.WHITE):
            hash = hash ^ self.castling_rights[0]

        if board.has_kingside_castling_rights(chess.BLACK):
            hash = hash ^ self.castling_rights[1]

        if board.has_queenside_castling_rights(chess.WHITE):
            hash = hash ^ self.castling_rights[2]

        if board.has_queenside_castling_rights(chess.BLACK):
            hash = hash ^ self.castling_rights[3]
        
        return hash
