import chess
import itertools
from game_info import GameInfo

class Evaluator:
    def __init__(self, evaluators):
        self.evaluators = evaluators

    def evaluate(self, board):
        score = 0
        for evaluator in self.evaluators:
            score += evaluator[0].evaluate(board) * evaluator[1]
        
        return score

class MaterialEvaluator:
    """
    One point is an equivalent of a value of a single pawn
    """
    PIECE_VALUES = {
        chess.PAWN: 1,
        chess.KNIGHT: 3,
        chess.BISHOP: 3,
        chess.ROOK: 5,
        chess.QUEEN: 9
    }

    def evaluate(self, board):
        return self.__get_material_value(board)
    
    def __get_material_value(self, board):
        return self.__get_material_value_for_side(board, chess.WHITE) - self.__get_material_value_for_side(board, chess.BLACK)
    
    def __get_material_value_for_side(self, board, side):
        sum = 0

        sum += len(board.pieces(chess.PAWN, side)) * self.PIECE_VALUES[chess.PAWN]
        sum += len(board.pieces(chess.KNIGHT, side)) * self.PIECE_VALUES[chess.KNIGHT]
        sum += len(board.pieces(chess.BISHOP, side)) * self.PIECE_VALUES[chess.BISHOP]
        sum += len(board.pieces(chess.ROOK, side)) * self.PIECE_VALUES[chess.ROOK]
        sum += len(board.pieces(chess.QUEEN, side)) * self.PIECE_VALUES[chess.QUEEN]

        return sum

class CenterControlEvaluator:
    """
    One point is an equivalent of one of the central squares attacked by one piece
    """

    def evaluate(self, board):
        return self.__get_center_control_for_side(board, chess.WHITE) - self.__get_center_control_for_side(board, chess.BLACK)
    
    def __get_center_control_for_side(self, board, side):
        return len(board.attackers(side, 27)) + len(board.attackers(side, 28)) + len(board.attackers(side, 35)) + len(board.attackers(side, 36))

class PieceSquareEvaluator:
    HUMAN_READABLE_PST = {
        chess.WHITE: {
            chess.PAWN: {
                'default': [
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 5,  5,  5,  5,  5,  5,  5,  5],
                    [ 2,  2,  2,  3,  3,  1,  1,  1],
                    [ 1,  1,  1,  2,  2,  1,  0,  0],
                    [ 0,  0,  2,  2,  2,  1,  0,  0],
                    [ 0,  0, -1,  0,  0, -2,  1,  1],
                    [ 0,  0,  0, -2, -2,  3,  3,  2],
                    [ 0,  0,  0,  0,  0,  0,  0,  0]
                ],
            },
            chess.KNIGHT: {
                'default': [
                    [-3, -2, -1, -1, -1, -1, -2, -3],
                    [-2,  1,  1,  1,  1,  1,  1, -2],
                    [-1,  2,  3,  3,  3,  3,  2, -1],
                    [-1,  1,  2,  3,  3,  2,  1, -1],
                    [-2,  1,  2,  2,  2,  2,  1, -2],
                    [-2,  0,  0,  0,  0,  0,  0, -2],
                    [-2, -1, -1, -1, -1, -1, -1, -2],
                    [-3, -2, -2, -2, -2, -2, -2, -3]
                ],
            },
            chess.BISHOP: {
                'default': [
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  1,  1,  1,  1,  0,  0],
                    [ 0,  0,  1,  1,  1,  1,  0,  0],
                    [ 0,  0,  1,  1,  1,  1,  0,  0],
                    [ 0,  1,  1,  1,  1,  1,  1,  0],
                    [ 0,  2,  0,  0,  0,  0,  2,  0],
                    [-1, -1, -1, -1, -1, -1, -1, -1]
                ],
            },
            chess.ROOK: {
                'default': [
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 1,  1,  1,  1,  1,  1,  1,  1],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0]
                ],
            },
            chess.KING: {
                'default': [
                    [-3, -3, -3, -3, -3, -3, -3, -3],
                    [-3, -3, -3, -3, -3, -3, -3, -3],
                    [-3, -3, -3, -3, -3, -3, -3, -3],
                    [-3, -3, -3, -3, -3, -3, -3, -3],
                    [-2, -2, -2, -2, -2, -2, -2, -2],
                    [-1, -1, -1, -1, -1, -1, -1, -1],
                    [ 1,  1,  1, -2, -2,  1,  1,  1],
                    [ 1,  3,  3, -1,  0,  1,  3,  2]
                ],
                'endgame': [
                    [-3, -2, -2, -2, -2, -2, -2, -3],
                    [-2,  0,  0,  0,  0,  0,  0, -2],
                    [-2,  0,  1,  2,  2,  1,  0, -2],
                    [-2,  0,  2,  3,  3,  2,  0, -2],
                    [-2,  0,  2,  3,  3,  2,  0, -2],
                    [-2,  0,  1,  2,  2,  1,  0, -2],
                    [-2,  0,  0,  0,  0,  0,  0, -2],
                    [-3, -2, -2, -2, -2, -2, -2, -3]
                ],
            },
            chess.QUEEN: {
                'default': [
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0]
                ]
            }
        },
        chess.BLACK: {
            chess.PAWN: {
                'default': [
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0, -2, -2,  3,  3,  2],
                    [ 0,  0, -1,  0,  0, -2,  1,  1],
                    [ 0,  0,  2,  2,  2,  1,  0,  0],
                    [ 1,  1,  1,  2,  2,  1,  0,  0],
                    [ 2,  2,  2,  3,  3,  1,  1,  1],
                    [ 5,  5,  5,  5,  5,  5,  5,  5],
                    [ 0,  0,  0,  0,  0,  0,  0,  0]
                ],
            },
            chess.KNIGHT: {
                'default': [
                    [-3, -2, -2, -2, -2, -2, -2, -3],
                    [-2, -1, -1, -1, -1, -1, -1, -2],
                    [-2,  0,  0,  0,  0,  0,  0, -2],
                    [-2,  1,  2,  2,  2,  2,  1, -2],
                    [-1,  1,  2,  3,  3,  2,  1, -1],
                    [-1,  2,  3,  3,  3,  3,  2, -1],
                    [-2,  1,  1,  1,  1,  1,  1, -2],
                    [-3, -2, -1, -1, -1, -1, -2, -3]
                ],
            },
            chess.BISHOP: {
                'default': [
                    [-1, -1, -1, -1, -1, -1, -1, -1],
                    [ 0,  2,  0,  0,  0,  0,  2,  0],
                    [ 0,  1,  1,  1,  1,  1,  1,  0],
                    [ 0,  0,  1,  1,  1,  1,  0,  0],
                    [ 0,  0,  1,  1,  1,  1,  0,  0],
                    [ 0,  0,  1,  1,  1,  1,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0]
                ],
            },
            chess.ROOK: {
                'default': [
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 1,  1,  1,  1,  1,  1,  1,  1],
                    [ 0,  0,  0,  0,  0,  0,  0,  0]
                ],
            },
            chess.KING: {
                'default': [
                    [ 1,  3,  3, -1,  0,  1,  3,  2],
                    [ 1,  1,  1, -2, -2,  1,  1,  1],
                    [-1, -1, -1, -1, -1, -1, -1, -1],
                    [-2, -2, -2, -2, -2, -2, -2, -2],
                    [-3, -3, -3, -3, -3, -3, -3, -3],
                    [-3, -3, -3, -3, -3, -3, -3, -3],
                    [-3, -3, -3, -3, -3, -3, -3, -3],
                    [-3, -3, -3, -3, -3, -3, -3, -3]
                ],
                'endgame': [
                    [-3, -2, -2, -2, -2, -2, -2, -3],
                    [-2,  0,  0,  0,  0,  0,  0, -2],
                    [-2,  0,  1,  2,  2,  1,  0, -2],
                    [-2,  0,  2,  3,  3,  2,  0, -2],
                    [-2,  0,  2,  3,  3,  2,  0, -2],
                    [-2,  0,  1,  2,  2,  1,  0, -2],
                    [-2,  0,  0,  0,  0,  0,  0, -2],
                    [-3, -2, -2, -2, -2, -2, -2, -3]
                ]
            },
            chess.QUEEN: {
                'default': [
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0],
                    [ 0,  0,  0,  0,  0,  0,  0,  0]
                ]
            }
        }
    }

    def __init__(self):
        self.PST = dict(self.HUMAN_READABLE_PST)

        for side in self.PST:
            for piece in self.PST[side]:
                for mode in self.PST[side][piece]:
                    self.PST[side][piece][mode].reverse()
                    self.PST[side][piece][mode] = [y for x in self.PST[side][piece][mode] for y in x]

    def evaluate(self, board):
        return self.__get_score_for_side(board, chess.WHITE) - self.__get_score_for_side(board, chess.BLACK)

    def __get_score_for_side(self, board, side):
        score = 0

        pieces = [
            (chess.PAWN, 'default'),
            (chess.KNIGHT, 'default'),
            (chess.BISHOP, 'default'),
            (chess.ROOK, 'default'),
            (chess.QUEEN, 'default'),
            (chess.KING, 'default' if not GameInfo.is_endgame(board) else 'endgame'),
        ]

        for piece, mode in pieces:
            score += sum(
                itertools.compress(
                    self.PST[side][piece][mode],
                    board.pieces(piece, side).tolist()
                )
            )

        return score

class TempoEvaluator:
    def evaluate(self, board):
        score = 1 - (board.fullmove_number * 0.1)

        return (score if score > 0 else 0) * (1 if board.turn == chess.WHITE else -1)

class MobilityEvaluator:
    def evaluate(self, board):
        side_to_move = len(list(board.pseudo_legal_moves))

        board.push(chess.Move.null())
        other_side = len(list(board.pseudo_legal_moves))
        board.pop()

        score = side_to_move - other_side

        return score if board.turn == chess.WHITE else -score
