import chess

class GameInfo:
    @staticmethod
    def is_endgame(board):
        score = GameInfo.__get_piece_score_for_side(board, chess.WHITE) + GameInfo.__get_piece_score_for_side(board, chess.BLACK)

        return True if score <= 20 else False

    def __get_piece_score_for_side(board, side):
        score = 0
        score += len(board.pieces(chess.KNIGHT, side))*3
        score += len(board.pieces(chess.BISHOP, side))*3
        score += len(board.pieces(chess.ROOK, side))*5
        score += len(board.pieces(chess.QUEEN, side))*9
        
        return score
