import unittest
import chess
import evaluation

test_data = [
    (chess.Board(), 0),
    (chess.Board(fen='k7/6pp/8/8/8/2b5/PP6/K7 w - -'), -9),
    (chess.Board(fen='k7/6pp/8/8/8/2b5/PP6/K7 b - -'), 9)
]

class TestMobilityEvaluator(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.evaluator = evaluation.MobilityEvaluator()

    def test_evaluator(self):
        for board, score in test_data:
            with self.subTest():
                self.assertEqual(self.evaluator.evaluate(board), score)

if __name__ == '__main__':
    unittest.main()