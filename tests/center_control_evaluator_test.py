import unittest
import chess
import evaluation

test_data = [
    (chess.Board(fen='k7/pp6/5n2/8/8/2B2B2/PP6/K7 w - -'), 2),
    (chess.Board(fen='k7/pp6/8/8/8/8/PP6/K7 w - -'), 0),
    (chess.Board(fen='k7/pp6/8/4q3/3Q4/8/PP6/K7 w - -'), 0),
    (chess.Board(fen='k7/pp6/8/8/3Q4/8/PP6/K7 w - -'), 3),
    (chess.Board(fen='4kb2/pn1r3p/1pn1p1pP/2p1NpP1/P1P2P2/2BPP3/3RK3/3R4 b - - 2 46'), -1)
]

class TestCenterControlEvaluator(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.evaluator = evaluation.CenterControlEvaluator()

    def test_evaluator(self):
        for board, score in test_data:
            with self.subTest():
                self.assertEqual(self.evaluator.evaluate(board), score)

if __name__ == '__main__':
    unittest.main()