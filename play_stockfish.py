import chess
import chess.pgn
import evaluation
import search
from pystockfish import *
import time
from zobrist import Zobrist
from hash_tables import TT
import sys

def get_stockfish_move(moves):
    stockfish.setposition(moves)
    return stockfish.bestmove()['move']

def get_my_move(board):

    start = time.time()
    move_uci = engine.calculate(board, int(sys.argv[1]), True)
    end = time.time()

    move_time = end - start

    print("Move time: %f" % (move_time))
    move_times.append(move_time)

    return move_uci

move_times = []

# Stockfish
stockfish = Engine(depth=10)

# "Me"
ev = evaluation.Evaluator([
    (evaluation.MaterialEvaluator(), 1),
    (evaluation.CenterControlEvaluator(), 0.2),
    (evaluation.PieceSquareEvaluator(), 0.2),
    (evaluation.TempoEvaluator(), 1),
    (evaluation.MobilityEvaluator(), 0.1),
])
zobrist = Zobrist()
tt = TT(zobrist)
engine = search.SimpleAlphaBeta(ev, tt)

# Starting position
board = chess.Board()
moves = []

if len(sys.argv) > 2:
    pgn = open(sys.argv[2])
    game = chess.pgn.read_game(pgn)

    board = game.board()
    for move in game.mainline_moves():
        board.push(move)

    moves = [move.uci() for move in board.move_stack]


# for PGN
game = chess.pgn.Game()
game.setup(board)
node = None

#exit()

is_my_turn = True

while True:
    # Get the best move
    move_uci = get_my_move(board) if is_my_turn else get_stockfish_move(moves)
    is_my_turn = not is_my_turn

    # Make move on chessboard
    moves.append(move_uci) # for stockfish

    move = chess.Move.from_uci(move_uci)
    board.push(move) # for "me"

    # Add move to PGN
    if node is None:
        node = game.add_variation(move)
    else:
        node = node.add_variation(move)

    #
    print(moves)

    if board.is_game_over():
        f = open("last.pgn", "w")
        f.write(str(game))
        f.close()

        print("__________________")
        print("Average move time: %f" % (sum(move_times) / len(move_times)))
        exit()