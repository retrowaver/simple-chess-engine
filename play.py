import chess
import evaluation
import search
import chess.pgn


board = chess.Board()
game = chess.pgn.Game()
node = None


ev = evaluation.Evaluator([
    (evaluation.MaterialEvaluator(), 1),
    (evaluation.CenterControlEvaluator(), 0.2),
    (evaluation.PieceSquareEvaluator(), 0.2)
])
engine = search.SimpleAlphaBeta(ev)


while True:
    move_uci = input("Make a move (uci): ")

    if move_uci == 'end' or board.is_game_over():
        f = open("last.pgn", "w")
        f.write(str(game))
        f.close()
        exit()


    move = chess.Move.from_uci(move_uci)

    if node is None:
        node = game.add_variation(move)
    else:
        node = node.add_variation(move)

    board.push(move)


    engine.alpha_beta(board, 4, board.turn, -9999, 9999, True)

    board.push(engine.best_move)
    node = node.add_variation(engine.best_move)

    print('Engine played: ' + engine.best_move.uci())