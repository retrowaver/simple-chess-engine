import chess

class TT:
    def __init__(self, zobrist):
        self.zobrist = zobrist
        self.table = {}

    def get_info(self, board):
        hash = self.zobrist.hash(board)

        if hash in self.table:
            return self.table[hash]

        return None

    def get_info_from_previous(self, board):
        hash = self.zobrist.hash(board)

        if hash in self.previous_table:
            return self.previous_table[hash]

        return None

    def save_info(self, board, score, flag, depth, moves):
        hash = self.zobrist.hash(board)
        self.table[hash] = {
            'score': score,
            'flag': flag,
            'depth': depth,
            'moves': moves
        }

    def prepare_table(self):
        self.previous_table = dict(self.table)
        self.table = {}

    def get_pv_moves(self, board):

        moves = []

        while self.zobrist.hash(board) in self.table:
            tt_entry = self.table[self.zobrist.hash(board)]

            #print("%s - %s" % (self.zobrist.hash(board), tt_entry['flag']))


            #if tt_entry['best_move'] == None:
                #print(self.table)
                #exit()


            moves.append(tt_entry['moves'][0][0].uci())

            #print(tt_entry['best_move'].uci())

            board.push(tt_entry['moves'][0][0])

        return moves

    def get_best_move(self, board):
        return self.table[self.zobrist.hash(board)]['moves'][0][0].uci()