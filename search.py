import chess

class SimpleAlphaBeta:
    def __init__(self, evaluator, tt):
        self.evaluator = evaluator
        self.tt = tt
        self.see = SEE()

        self.counter = 0

        self.last_iteration_score = 0

    def calculate(self, board, depth, verbose = False):
        initial_board = board.copy()

        for depth in range(1, depth + 1):

            self.tt.prepare_table()

            score = self.negamax(board, depth, -99999, 99999) * (-1 if board.turn == chess.BLACK else 1)
            self.last_iteration_score = score

            if verbose:
                print("%.2f (depth %d) %s" % (
                    score,
                    depth,
                    ' '.join(self.tt.get_pv_moves(initial_board.copy()))
                ))

        return self.tt.get_best_move(initial_board.copy())

    def negamax(self, board, depth, alpha, beta):
        self.counter += 1

        if board.is_game_over():
            if board.result() == '1-0':
                return 9999 * (1 if board.turn == chess.WHITE else -1)
            elif board.result() == '0-1':
                return -9999 * (1 if board.turn == chess.WHITE else -1)
            else:
                return 0

        if depth == 0:
            score = self.evaluator.evaluate(board) + self.see.get_score(board)
            return score * (1 if board.turn == chess.WHITE else -1)

        # Get values from TT
        alpha_orig = alpha
        tt_entry = self.tt.get_info(board)
        if tt_entry != None and tt_entry['depth'] >= depth:
            if tt_entry['flag'] == 'EXACT':
                return tt_entry['score']
            elif tt_entry['flag'] == 'LOWERBOUND':
                alpha = max(alpha, tt_entry['score'])
            elif tt_entry['flag'] == 'UPPERBOUND':
                beta = min(beta, tt_entry['score'])

            if alpha >= beta:
                return tt_entry['score']

        # Main loop
        moves = self.get_ordered_moves(board)
        moves_evals = []
        value = -99999
        for move in moves:
            if alpha >= beta:
                # Beta cutoff, but append the moves
                moves_evals.append((move, self.last_iteration_score))
                continue

            board.push(move)

            score = -self.negamax(board, depth - 1, -beta, -alpha)
            moves_evals.append((move, score))

            value = max(value, score)
            alpha = max(alpha, value)

            board.pop()
        
        # Save data to TT
        if value <= alpha_orig:
            flag = 'UPPERBOUND'
        elif value >= beta:
            flag = 'LOWERBOUND'
        else:
            flag = 'EXACT'

        self.tt.save_info(
            board,
            value,
            flag,
            depth,
            sorted(moves_evals, key=lambda tup: tup[1], reverse=True) if flag == 'EXACT' or flag == 'LOWERBOUND' else []
        )

        return value

    def get_ordered_moves(self, board):
        tt_entry = self.tt.get_info_from_previous(board)

        if tt_entry != None and tt_entry['moves']:
            return [move[0] for move in tt_entry['moves']]

        return board.legal_moves
        
class SEE:
    SEE_PIECE_VALUES = {
        chess.PAWN: 1,
        chess.KNIGHT: 3,
        chess.BISHOP: 3,
        chess.ROOK: 5,
        chess.QUEEN: 9,
        chess.KING: 9999
    }

    def get_score(self, initial_board):
        board = initial_board.copy()

        target_square = board.peek().to_square
        defender_value = self.SEE_PIECE_VALUES[board.piece_at(target_square).piece_type]
        attackers = self.__get_sorted_attackers(board, target_square)

        score = 0

        while len(attackers) > 0:
            lowest_attacker = attackers.pop()
            score += defender_value * (1 if board.turn == chess.WHITE else -1)
            defender_value = lowest_attacker[1]

            board.push(lowest_attacker[0])
            attackers = self.__get_sorted_attackers(board, target_square)

        return max(0, score) if initial_board.turn == chess.WHITE else min(0, score)

    def __get_sorted_attackers(self, board, target_square):
        attackers = []

        for move in board.legal_moves:
            if move.to_square == target_square:
                attackers.append((
                    move,
                    self.SEE_PIECE_VALUES[board.piece_type_at(move.from_square)]
                ))

        return sorted(attackers, key=lambda tup: tup[1], reverse=True)